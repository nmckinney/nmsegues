//
//  NMFadeSegue.h
//  NMSegues
//
//  Created by Nicholas McKinney on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMFadeSegue : UIStoryboardSegue {
/*
 @param fadeIn: The duration for the fade-in animation of the destination view controller.
 @param fadeOut: The duration for the fade-in animation of the source view controller.
 @param navController: Boolean that shows whether the view controller segueing has a navigation controller.
*/
    float fadeIn;
    float fadeOut;
    bool navController;
}

@property (readonly) UIViewController *src;
@property (readonly) UIViewController *dest;


/*
 @param navControllerPresent: Value stored in ivar (bool)navController.
 @param fadeOutDuration: Value stored in ivar (float)fadeOut.
 @param fadeInDuration: Value stored in ivar (float)fadeIn.
*/
-(id) initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination transitionFromNavController:(bool)navControllerPresent fadeOutDuration:(float)fadeOutDuration fadeInDuration:(float)fadeInDuration;

@end
