//
//  NMPushSegue.m
//  NMSegues
//
//  Created by Nicholas McKinney on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NMPushSegue.h"

@implementation NMPushSegue

-(id) initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination andPushDirection:(NMPUSH_DIRECTION)direction duration:(float)duration
{
    if (self = [super init]) {
        NSAssert(!src.navigationController,@"This class is for views without a navigation controller");
        src = source;
        dest = destination;
        
        if (!direction) {
            pushDirection = NMPUSH_FORWARD;
        }
        
        if (!duration) {
            pushDuration = 0.25;
        }
    }
    return self;
}

-(void) perform
{
    CGRect originalFrame = dest.view.frame;
    dest.view.frame = CGRectMake(dest.view.frame.size.width, 
                                 dest.view.frame.origin.y, 
                                 dest.view.frame.size.width, 
                                 dest.view.frame.size.height);
    [src presentViewController:dest animated:NO completion:NULL];

    [UIView animateWithDuration:pushDuration animations:^{
        dest.view.frame = originalFrame;
    }];
}

@end
