//
//  NMFadeSegue.m
//  NMSegues
//
//  Created by Nicholas McKinney on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NMFadeSegue.h"
#import <QuartzCore/QuartzCore.h>

@implementation NMFadeSegue
@synthesize src,dest;


-(id) initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination transitionFromNavController:(bool)navControllerPresent fadeOutDuration:(float)fadeOutDuration fadeInDuration:(float)fadeInDuration
/*
 Contructor method passed upon the creation of an NMFadeSegue object that will be used to set the segue pointer in 
 the "prepareForSegue" method of a view controller.
 */
{
    //Set default settings and set values to ivars
    if (self = [super init]) {
        
        if (!fadeOutDuration) {
            fadeOut = 1;
        }
        if (!fadeInDuration) {
            fadeIn = fadeOut;
        }
        src = source;
        dest = destination;
        
        navController = navControllerPresent;
    }
    return self;
}

-(void) perform
{
    src.view.layer.opacity = 1;
    dest.view.layer.opacity = 0;
    
    [UIView animateWithDuration:fadeOut animations:^{
        src.view.layer.opacity = 0;
        
        if (navController) {
            [src.navigationController pushViewController:dest animated:NO];
        }
        else {
            [src presentViewController:dest animated:NO completion:NULL];
        }
    }  completion:^(BOOL completed){
        
        [UIView animateWithDuration:fadeIn animations:^{
            dest.view.layer.opacity = 1;
        }];
        
    }];
}

@end
