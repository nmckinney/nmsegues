//
//  NMPushSegue.h
//  NMSegues
//
//  Created by Nicholas McKinney on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
/*
 NMPushSegue is used mainly due to the fact that the push segue cannot be used without a navigation controller.
 Note: This class can ONLY be used by view controllers without a navigation controller.
 */

typedef enum {
    NMPUSH_FORWARD = 1,
    NMPUSH_BACKWARD
}NMPUSH_DIRECTION;

@interface NMPushSegue : UIStoryboardSegue {
    UIViewController *src;
    UIViewController *dest;
    NMPUSH_DIRECTION pushDirection;
    float pushDuration;
}

-(id) initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination andPushDirection: (NMPUSH_DIRECTION) direction duration: (float) duration;

@end
