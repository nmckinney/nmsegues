//
//  NMFlyInSegue.m
//  NMSegues
//
//  Created by Nicholas McKinney on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NMFlyInSegue.h"

@implementation NMFlyInSegue

-(id) initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination transitionFromNavController:(bool)navControllerPresent duration:(float)flyInDuration direction:(FLY_IN_DIRECTION)flyInDirection
{
    if (self = [super init]) {
        if (!flyInDirection) {
            direction = NMFlyInTopLeft;
        }
        if (!flyInDuration) {
            flyInDuration = 1;
        }
        navController = navControllerPresent;
        
        src = source;
        dest = destination;
    }
    return self;
}

-(void) perform
{
    CGRect frame = dest.view.frame;
    CGRect changedFrame;
    switch (direction) {
        case NMFlyInTopLeft:
            changedFrame = CGRectMake(src.view.frame.size.width * (-1), 
                               src.view.frame.size.height * (-1), 
                               src.view.frame.size.width, 
                               src.view.frame.size.height);
            break;
        case NMFlyInTopRight:
            changedFrame = CGRectMake(src.view.frame.size.width, 
                               src.view.frame.size.height * (-1), 
                               src.view.frame.size.width, 
                               src.view.frame.size.height);
            break;
        case NMFlyInBottomLeft:
            changedFrame = CGRectMake(src.view.frame.size.width * (-1), 
                               src.view.frame.size.height, 
                               src.view.frame.size.width, 
                               src.view.frame.size.height);
            break;
        case NMFlyInBottomRight:
            changedFrame = CGRectMake(src.view.frame.size.width, 
                               src.view.frame.size.height, 
                               src.view.frame.size.width, 
                               src.view.frame.size.height);
            break;
        default:
            break;
    }
    
    dest.view.frame = frame;
    
    if (navController) {
        [src.navigationController pushViewController:dest animated:NO];
    }
    else {
        [src presentViewController:dest animated:NO completion:NULL];
    }
    
    [UIView animateWithDuration:duration animations:^{
        dest.view.frame = frame;
    }];

}

@end
