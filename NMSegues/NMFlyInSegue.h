//
//  NMFlyInSegue.h
//  NMSegues
//
//  Created by Nicholas McKinney on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    NMFlyInTopLeft = 1,
    NMFlyInTopRight,
    NMFlyInBottomLeft,
    NMFlyInBottomRight
}FLY_IN_DIRECTION;

@interface NMFlyInSegue : UIStoryboardSegue {
    bool navController;
    float duration;
    FLY_IN_DIRECTION direction;
    UIViewController *src;
    UIViewController *dest;
}

-(id) initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination transitionFromNavController:(bool)navControllerPresent duration:(float)flyInDuration direction:(FLY_IN_DIRECTION)flyInDirection

@end
